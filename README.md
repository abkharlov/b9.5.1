# Array Function

A light-weigted array function library.

# Installation

`npm install sf_B9.5.1

# Why to use

It has no dependencies.

# Doc

```
const arrayFunction = require("simple-array-functions");

const isEven = arrayFunction.isEven;
const isOdd = arrayFunction.isOdd;
const extractEvenNumbers = arrayFunction.extractEvenNumbers;
const extractOddNumbers = arrayFunction.extractOddNumbers;

/*----------- Test for isEven ---------------*/

assert.equal(isEven(0), true);
assert.equal(isEven(2), true);
assert.equal(isEven(1), false);

assert.equal(isEven(-11), false);
assert.equal(isEven(12), true);

/*----------- Test for isOdd ---------------*/

assert.equal(isOdd(0), false);
assert.equal(isOdd(2), false);
assert.equal(isOdd(1), true);

assert.equal(isOdd(-11), true);
assert.equal(isOdd(12), false);

/*----------- Test for extractEvenNumbers ---------------*/

assert.deepEqual(extractEvenNumbers([1]),[]);
assert.deepEqual(extractEvenNumbers([0]),[0]);

assert.deepEqual(extractEvenNumbers([1,2]),[2]);
assert.deepEqual(extractEvenNumbers([1,2,3,4]),[2,4]);

assert.deepEqual(extractEvenNumbers([-1,-2]),[-2]);
assert.deepEqual(extractEvenNumbers([1,-12]),[-12]);

/*----------- Test for extractOddNumbers ---------------*/

assert.deepEqual(extractOddNumbers([1]),[1]);
assert.deepEqual(extractOddNumbers([0]),[]);

assert.deepEqual(extractOddNumbers([1,2]),[1]);
assert.deepEqual(extractOddNumbers([1,2,3,4]),[1,3]);

assert.deepEqual(extractOddNumbers([-1,-2]),[-1]);
assert.deepEqual(extractOddNumbers([1,-12]),[1]);

```
