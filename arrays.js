const isEven = function(number){
  return Math.abs(number % 2) == 0;
};

const isOdd = function(number){
  return Math.abs(number % 2) == 1;
};

const extractOddNumbers = function(list){
   return list.filter(isOdd);
};

const extractEvenNumbers = function(list){
   return list.filter(isEven);
};


exports.isEven = isEven;
exports.isOdd = isOdd;
exports.extractEvenNumbers = extractEvenNumbers;
exports.extractOddNumbers = extractOddNumbers;
